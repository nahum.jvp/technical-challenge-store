DROP TABLE IF EXISTS product;
CREATE TABLE product(
    id TINYINT unsigned NOT NULL AUTO_INCREMENT,
    title VARCHAR(30) NOT NULL,
    sku VARCHAR(30) NOT NULL,
    price float unsigned NOT NULL,
    stock INT unsigned NULL,
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME  NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    deleted_at DATETIME  NULL DEFAULT NULL,
    CONSTRAINT pk_product_id PRIMARY KEY(id)
);
ALTER TABLE product
ADD UNIQUE (sku);

DROP TABLE IF EXISTS purchase_order;
CREATE TABLE purchase_order(
    id TINYINT unsigned NOT NULL AUTO_INCREMENT,
    delivery_at DATETIME NULL DEFAULT NULL,
    send_at DATETIME DEFAULT NULL,
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME  NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    deleted_at DATETIME  NULL DEFAULT NULL,
    CONSTRAINT pk_order_id PRIMARY KEY(id)
);
ALTER TABLE purchase_order
ADD CONSTRAINT date_check CHECK (send_at <= delivery_at);

DROP TABLE IF EXISTS order_product;
CREATE TABLE order_product(
    order_id TINYINT unsigned NOT NULL,
    product_id TINYINT unsigned NOT NULL,
    amount smallint unsigned NOT NULL,
    price float unsigned NOT NULL,
    CONSTRAINT pk_order_product_order_id_product_id PRIMARY KEY (order_id, product_id),
    CONSTRAINT fk_order_product_order_id FOREIGN KEY (order_id) REFERENCES purchase_order(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_order_product_product_id FOREIGN KEY (product_id) REFERENCES product(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT uq_order_id_product_id UNIQUE (order_id, product_id)
);
