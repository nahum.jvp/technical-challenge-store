"""Product model"""
from datetime import datetime
from aplicacion import db

class Product(db.Model):
    """Class of the table Product"""

    __tablename__  = 'product'
    id = db.Column(db.Integer, primary_key= True)
    title = db.Column(db.String(30), nullable=False)
    sku = db.Column(db.String(30), nullable=False)
    price = db.Column(db.Float , nullable=False)
    stock = db.Column(db.Integer, nullable= False)
    created_at = db.Column(db.DateTime, nullable=True)
    updated_at = db.Column(db.DateTime, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)

    def get_find_id(self, id):
        """find by id

        Args:
            id (int): id of the order to be searched for

        Returns:
            object: returns the corresponding object if it is found,
            otherwise it returns None
        """
        return self.query.filter_by(id = id, deleted_at=None).first()

    def get_all(self, params=None):
        """Get all non deleted order

        Returns:
            object: returns the corresponding object if it is found,
            otherwise it returns None
        """
        return self.query.order_by(params).filter_by(deleted_at=None).all()

    def exist_sku(self, sku):
        """checks if the sku already exists in the database

        Args:
            sku (String): product SKU

        Returns:
            Object: object corresponding to the sku if it exists,
            otherwise returns None
        """
        return self.query.filter_by(sku = sku).first()

    def create(self):
        """created a new product

        Returns:
            dictionary: returns a dictionary with the resulting data
        """
        self.created_at = datetime.now().strftime("%Y-%m_%d %H:%M:%S")
        db.session.add(self)
        db.session.commit()
        diccionario = {
            "id": self.id,
            "title": self.title,
            "sku": self.sku,
            "price": self.price,
            "stock": self.stock
        }
        return  diccionario

    def update(self, id, params):
        """update product with params in db

        Args:
            id (int): id of the product to be searched for
            params (dictionary): dictionary with all fields 

        Returns:
            dictionary: returns a dictionary with the resulting data
        """
        db.session.expunge_all()
        product = db.session.query(Product).filter(Product.id==id, Product.deleted_at==None).first()
        if not product:
            return False
        product.title = params.get('title', None)
        product.sku = params.get('sku', None)
        product.price = params.get('price', None)
        product.stock = params.get('stock', None)
        db.session.commit()
        diccionario = {
            "id": product.id,
            "title": product.title,
            "sku": product.sku,
            "price": product.price,
            "stock": product.stock
        }
        return  diccionario

    def delete(self, id):
        """logical deletion of the product

        Args:
            id (int): id of the product to be searched for

        Returns:
            booleand: true if it could eliminate otherwise it would return false
        """
        product = db.session.query(Product).filter(Product.id==id, Product.deleted_at==None).first()
        if product:
            product.deleted_at = datetime.now().strftime("%Y-%m_%d %H:%M:%S")
            db.session.commit()
            return True
        return False

    def destroy(self, id):
        """physical deletion, the product is deleted directly to the database

        Args:
            id (int): id of the order to be searched for

        Returns:
            booleand: true if it could eliminate otherwise it would return false
        """
        product = db.session.query(Product).filter_by(id=id).first()
        if product:
            db.session.delete(product)
            db.session.commit()
            return True
        return None

    def toggle_status(self, id):
        """a previously deleted product is restored logically

        Args:
            id (int): id of the product to be searched for

        Returns:
            booleand: true if it could restored otherwise it would return false
        """
        product = (db.session.query(Product).filter(Product.id==id, Product.deleted_at!=None).first())
        if product:
            product.deleted_at = None
            db.session.commit()
            return True
        return False
