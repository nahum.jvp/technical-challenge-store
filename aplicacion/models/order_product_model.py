"""Order Product model"""
from aplicacion import db

class Order_Product(db.Model):
    """Class of the table Order_Product"""

    __tablename__  = 'order_product'
    order_id = db.Column(db.Integer, db.ForeignKey('purchase_order.id'),primary_key= True)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'), primary_key= True)
    amount = db.Column(db.Integer, nullable=False)
    price = db.Column(db.Float , nullable=False)

    def create(self):
        """add a new product to order

        Returns:
            dictionary: returns a dictionary with the resulting data
        """
        db.session.add(self)
        db.session.commit()
        diccionario = {
            "order_id": self.order_id,
            "product_id": self.product_id,
            "amount": self.amount,
            "price": self.price
        }
        return  diccionario

    def get_find_by(self, order_id, product_id):
        """to look for the relationship between product and order

        Args:
            order_id (int): id of the order to be searched for
            product_id (int): id of the product to be searched for

        Returns:
            object: returns the corresponding object if it is found,
            otherwise it returns None
        """
        return self.query.filter_by(order_id = order_id, product_id = product_id).first()

    def get_products_in_order(self, order_id):
        """the products registered in the order are obtained, identified by the order id

        Args:
            order_id (int): id of the order to be searched for

        Returns:
            list: returns a list of dictionaries belonging to each product
        """
        products = self.query.filter_by(order_id = order_id).all()
        list_products = []
        for product in products:
            diccionario = {
                "order_id": product.order_id,
                "product_id": product.product_id,
                "amount": product.amount,
                "price": product.price,
            }
            list_products.append(diccionario)
        return list_products

    def update(self, id, params):
        """update product in order with params in db

        Args:
            id (int): id of the order to be searched for
            params (dictionary): dictionary with all fields 

        Returns:
            dictionary: returns a dictionary with the resulting data
        """
        db.session.expunge_all()
        order_product = db.session.query(Order_Product).filter(Order_Product.id==id).first()
        if not order_product:
            return False
        order_product.order_id = params.get('order_id', None)
        order_product.product_id = params.get('product_id', None)
        order_product.amount = params.get('amount', None)
        db.session.commit()
        diccionario = {
            "order_id": order_product.order_id,
            "product_id": order_product.product_id,
            "amount": order_product.amount,
            "price": order_product.price,
        }
        return  diccionario
