"""Order model"""
from datetime import datetime
from aplicacion import db
from aplicacion.models.order_product_model import Order_Product
from aplicacion.models.product_model import Product

class Order(db.Model):
    """Class of the table purchase_order"""

    __tablename__  = 'purchase_order'
    id = db.Column(db.Integer, primary_key= True)
    delivery_at = db.Column(db.DateTime, nullable=True)
    send_at = db.Column(db.DateTime, nullable=True)
    created_at = db.Column(db.DateTime, nullable=True)
    updated_at = db.Column(db.DateTime, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)

    def get_find_id(self, id):
        """find by id

        Args:
            id (int): id of the order to be searched for

        Returns:
            object: returns the corresponding object if it is found,
            otherwise it returns None
        """
        return self.query.filter_by(id = id, deleted_at=None).first()
    
    def get_all(self):
        """Get all non deleted order

        Returns:
            object: returns the corresponding object if it is found,
            otherwise it returns None
        """
        return self.query.filter_by(deleted_at=None).all()

    def create(self):
        """created a new order
        Returns:
            dictionary: returns a dictionary with the resulting data
        """
        self.created_at = datetime.now().strftime("%Y-%m_%d %H:%M:%S")
        db.session.add(self)
        db.session.commit()
        diccionario = {
            "id": self.id,
            "send_at": self.send_at,
            "deleted_at": self.deleted_at,
        }
        return diccionario

    def update(self, id, params):
        """update order with params in db

        Args:
            id (int): id of the order to be searched for
            params (dictionary): dictionary with all fields 

        Returns:
            dictionary: returns a dictionary with the resulting data
        """
        db.session.expunge_all()
        order = db.session.query(Order).filter(Order.id==id, Order.deleted_at==None).first()
        if not order:
            return False
        order.title = params.get('title', None)
        order.sku = params.get('sku', None)
        order.price = params.get('price', None)
        order.stock = params.get('stock', None)
        db.session.commit()
        diccionario = {
            "id": order.id,
            "title": order.title,
            "sku": order.sku,
            "price": order.price,
            "stock": order.stock
        }
        return  diccionario

    def delete(self, id):
        """logical deletion of the order

        Args:
            id (int): id of the order to be searched for

        Returns:
            booleand: true if it could eliminate otherwise it would return false
        """
        order = db.session.query(Order).filter(Order.id==id, Order.deleted_at==None).first()
        if order:
            order.deleted_at = datetime.now().strftime("%Y-%m_%d %H:%M:%S")
            db.session.commit()
            return True
        return False

    def destroy(self, id):
        """physical deletion, the order is deleted directly to the database

        Args:
            id (int): id of the order to be searched for

        Returns:
            booleand: true if it could eliminate otherwise it would return false
        """
        order = db.session.query(Order).filter_by(id=id).first()
        if order:
            db.session.delete(order)
            db.session.commit()
            return True
        return None

    def toggle_status(self, id):
        """a previously deleted order is restored logically

        Args:
            id (int): id of the order to be searched for

        Returns:
            booleand: true if it could restored otherwise it would return false
        """
        order = db.session.query(Order).filter(Order.id==id, Order.deleted_at!=None).first()
        if order:
            order.deleted_at = None
            db.session.commit()
            return True
        return False

    def send_order(self, id):
        """order is sent and current date is recorded

        Args:
            id (int): id of the order to be searched for

        Returns:
            booleand: true if it could register otherwise it would return false
        """
        order = self.query.filter_by(id = id, deleted_at = None, send_at = None).first()
        if order:
            order_product = Order_Product().get_products_in_order(id)
            for product_add in order_product:
                product = Product().get_find_id(product_add.get("product_id", 0))
                product.stock = product.stock-product_add.get("amount", 0)
                db.session.commit()
            order.send_at = datetime.now().strftime("%Y-%m_%d %H:%M:%S")
            db.session.commit()
            return True
        return False

    def delivery_order(self, id):
        """the order was received and the date of receipt is saved

        Args:
            id (int): id of the order to be searched for

        Returns:
            booleand: true if it could register otherwise it would return false
        """
        order = self.query.filter_by(id = id, deleted_at = None, delivery_at = None).first()
        if order:
            order.delivery_at = datetime.now().strftime("%Y-%m_%d %H:%M:%S")
            db.session.commit()
            return True
        return False
