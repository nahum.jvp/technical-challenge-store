from os import error
from flask import(
    render_template, Blueprint, jsonify, request
)
from aplicacion.models.product_model import Product
from aplicacion.models.order_model import Order
from aplicacion.models.order_product_model import Order_Product
from aplicacion import db

order = Blueprint('orders', __name__)
product = Product()

@order.route('/orders', methods=["GET","POST"])
def get_all():
    """geting all products or create a new product
    """
    db.session.commit()
    if request.method == "GET":
        atributo = request.args.get('atributo', None)
        if atributo:
            atributo = atributo.lower()
            if atributo not in ["title", "sku"]:
                return(jsonify({'message':"Error in query params"})), 400
        produc = Product().get_all(atributo)
        productos = []
        for fila in produc:
            dicc = {
                'id': fila.id,
                'title': fila.title,
                'sku': fila.sku,
                'price': fila.price,
                'stock': fila.stock
            }
            productos.append(dicc)
        return render_template('principal/order.html', productos = productos)

    if request.method == "POST":
        json_data = request.json
        produc = Product()
        produc.sku = json_data['sku'].upper()
        if produc.exist_sku(produc.sku ):
            return(jsonify({'message':"SKU already exist"})), 400
        if json_data['price'] < 0 or json_data['stock'] < 0:
            return(jsonify({'message':"price and stock cannot be less than 0"})), 400
        produc.price = json_data['price']
        produc.stock = json_data['stock']
        produc.title = json_data['title']
        product_final = produc.create()
        if product_final:
            return(jsonify({'product': product_final,'message':"Product created"}))
        return(jsonify({'message':"Error in params"})), 400
    return None

@order.route('/delete_product/<id>', methods=["DELETE"])
def delete_product(id):
    """logical deletion of the product

    Args:
        id (string): product id
    """
    db.session.commit()
    if request.method == "DELETE":
        delete = product.delete(id)
        if delete:
            return (jsonify({'message':"was successfully eliminated"}))
        return (jsonify({'message':"could not be eliminated"}))
    return None

@order.route('/restore_product/<id>', methods=["PATCH"])
def restore_product(id):
    """restore a product

    Args:
        id (string): product id

    """
    db.session.commit()
    if request.method == "PATCH":
        destroy = product.toggle_status(id)
        if destroy:
            return (jsonify({'message':"was successfully restored"}))
        return (jsonify({'message':"could not be restored"}))
    return None

@order.route('/destroy_product/<id>', methods=["DELETE"])
def destroy_product(id):
    """removes the product from the database

    Args:
        id (string): id for product
    """
    db.session.commit()
    if request.method == "DELETE":
        delete = product.destroy(id)
        if delete:
            return (jsonify({'message':"was successfully destroyed"}))
        return (jsonify({'message':"could not be destroyed"}))
    return None

@order.route('/update_product/<id>', methods=["PUT"])
def update_product(id):
    """update product fields

    Args:
        id (string): id for product
    """
    db.session.commit()
    if request.method == "PUT":
        json_data = request.json
        sku = json_data['sku'].upper()
        produc = Product().exist_sku(sku)
        if produc and produc.id is not int(id):
            return(jsonify({'message':"SKU already exist"})), 400
        if json_data['price'] < 0 or json_data['stock'] < 0:
            return(jsonify({'message':"price and stock cannot be less than 0"})), 400
        produc = {
            "sku": sku,
            "price": json_data['price'],
            "stock": json_data['stock'],
            "title": json_data['title']
        }
        update = product.update(id, produc)
        if update:
            return (jsonify({'message':"was successfully updated"}))
        return (jsonify({'message':"could not be update"}))
    return None

@order.route('/info_orders', methods=["GET","POST"])
def info_orders():
    """view all orders with products
    """
    db.session.commit()
    if request.method == "GET":
        order = Order().get_all()
        orders = []
        for fila in order:
            products_list = Order_Product().get_products_in_order(fila.id)
            dicc = {
                'id': fila.id,
                'delivery_at': fila.delivery_at,
                'send_at': fila.send_at,
                'product': products_list
            }
            orders.append(dicc)
        if orders == []:
            return(jsonify({'orders': orders,'message':"Orders listed"})),204
        return(jsonify({'orders': orders,'message':"Orders listed"}))
    if request.method == "POST":
        order = Order()
        order_created = order.create()
        if order_created:
            return(jsonify({'Order': order_created,'message':"Order created"}))
        return(jsonify({'message':"Error in params"})), 400
    return None

@order.route('/order_product', methods=["POST"])
def register_order_product():
    """A product is assigned to an order with quantity
    """
    if request.method == "POST":
        json_data = request.json
        id_product = json_data['product_id']
        id_order = json_data['order_id']
        amount = json_data['amount']
        product = Product().get_find_id(id_product)
        order = Order().get_find_id(id_order)
        order_product = Order_Product()
        if not product or not order:
            return(jsonify({'message':"order_id or product_id not exist"})), 400
        exist = order_product.get_find_by(id_order, id_product)
        if order.send_at is not None:
            return(jsonify({'message':"the order has already been sent"})), 400
        if exist:
            return(jsonify({'message':"there is already an order with that product"})), 400
        if amount > product.stock or amount < 1:
            return(jsonify({'message':"there is not enough stock or the quantity is less than zero"})), 400
        order_product.product_id = id_product
        order_product.order_id = id_order
        order_product.amount = amount
        order_product.price = product.price
        add_product = order_product.create()
        if add_product:
            return(jsonify({'product': add_product,'message':"Product created"}))
        return(jsonify({'message':"Error in params"})), 400
    return None

@order.route('/send_order/<id>', methods=["PATCH"])
def send_order(id):
    """the shipment of the order is registered

    Args:
        id (string): order id
    """
    db.session.commit()
    if request.method == "PATCH":
        send_order = Order().send_order(id)
        print(send_order)
        if send_order:
            return (jsonify({'message':"send order successfully"}))
        return (jsonify({'message':"could not be send order"}))
    return None

@order.route('/delivery_order/<id>', methods=["PATCH"])
def delivery_order(id):
    """delivery is recorded 

    Args:
        id (string): order id
    """
    db.session.commit()
    if request.method == "PATCH":
        send_order = Order().delivery_order(id)
        print(send_order)
        if send_order:
            return (jsonify({'message':"successful delivery of the order was recorded"}))
        return (jsonify({'message':"could not register delivery"}))
    return None
