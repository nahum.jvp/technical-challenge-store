from flask import(
    render_template, Blueprint
)
from aplicacion import db

principal = Blueprint('principal', __name__)

@principal.route("/")
def index():
    db.session.commit()
    return render_template('principal/index.html')
