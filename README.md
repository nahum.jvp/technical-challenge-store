# Página para manejar inventarios


# La base de datos deberá ser la siguiente

En caso de querer cambiar los siguientes valores, ir al archivo config.py en la linea 5

host = 'localhost'
user = 'root'
password = ''
data base name = 'store_inventory_db'
port = 3309

# Base de datos

Xamp
cambie al puerto 3309

crear una base de datos con el nombre store_inventory_db selecionarla y correr
el archivo script_sql.sql

# Para iniciar xamp desde ubuntu

iniciar xamp 
sudo /opt/lampp/lampp start

# configurar el enviroment 

Para crear el entorno virtual con el que trabajaremos debemos tener python 3.9.0 instalado
después vamos a escribir los siguientes comandos

# instalar virtualenv en python

pip install virtualenv

# Para Windows

# escribir el siguiente comando para crear el enviroment con el que trabajaremos

virtualenv -p python3 env

# entraremos en nuestro enviroment 

.\env\bin source activate
.\env\Scripts\activate

# Para Ubuntu

# Para iniciar env

cd env
cd bin
source activate envname

Es importante volver a salir de estas carpetas

# Una ves dentro del (env)

VAmos a correr el siguiente comando

pip3 install -r requirements.txt

# Para levantar el proyecto

Usamos el siguiente comando, para necesitamos estar al nivel del archivo main.py

python main.py

# Descripción

Requerimos que se desarrolle un proyecto que permita controlar el inventario de una tienda.
Para eso es necesario contar con un registro de pedidos de productos.
Los productos tienen por lo menos los siguientes atributos:

1. Título
2. SKU
3. Precio de venta

Se deberá considerar que en cada pedido es posible agregar N cantidad de productos.
Al crear los pedidos se debe descontar la cantidad disponible de los productos en el
inventario.

# Desafíos

# Nivel 1
Programa usando algún framework de backend que cumpla con lo descrito anteriormente.
# Nivel 2
Crear endpoint GET /orders para consultar el inventario actual permitiendo filtrar por el
atributo SKU o título.
# Nivel 3
Crear frontend que integre el endpoint /orders para mostrar el inventario actual.

# Entregable
1. Repositorio GitLab con el código del proyecto
2. README.md con lo necesario para ejecutarlo en local

# ¿Qué evaluaremos?

Criterio Valor
Patrones de diseño 25%
Mejores prácticas 25%
Seguridad 25%
Código limpio 25%